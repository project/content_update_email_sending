<?php

namespace Drupal\content_update_email_sending\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Content Update Email Sending settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'content_update_email_sending_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['content_update_email_sending.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Default settings.
    $config = $this->config('content_update_email_sending.settings');

    $form['enable_emails'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable mail sending'),
      '#default_value' => $config->get('enable_emails'),
      '#description' => $this->t('Send an email when content is created or modified.'),
    ];

    $email_address = !empty($config->get('notify_content_email')) ?
      $config->get('notify_content_email') : 'noreply@website.com';
    $form['notify_email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email address'),
      '#default_value' => $email_address,
      '#description' => $this->t('The email address to send notifications to when content is created or modified.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('content_update_email_sending.settings');
    $config->set('enable_emails', $form_state->getValue('enable_emails'));
    $config->set('notify_content_email', $form_state->getValue('notify_email'));
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
